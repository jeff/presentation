% K8s
% Nous
% 21 03 2018

## Introduction

### But du lab

 * Premier contact avec k8s
 * Outillage pour le développeur
 * Bonnes pratiques

### Nous

 * @jflabbe
 * @glivron
 * @jgoblet
 * @srevereault

### Setup initial

http://192.168.1.120:xxxx/

## Kubernetes

### Vision logique
![k8s-logical](img/k8s-archi.png)


<aside class="notes">
Sylvain
</aside>


### Intérêts

### Option de déploiement

## Kubectl
### Commandes utiles
<aside class="notes">
Guillaume
</aside>
### Fichiers yaml

## Kompose
<aside class="notes">
Sylvain
</aside>

## Helm
### principes
utilité

<aside class="notes">
Sylvain
</aside>
### Kompose
### variabilisation

## Telepresence
###
<aside class="notes">
Jeff
</aside>
![](img/telepresence.svg)

### principes
utilité

https://www.telepresence.io/

### TP

## Draft
### principes
utilité
<aside class="notes">
Sylvain
</aside>

### TP

## Bonnes pratiques
### health check
<aside class="notes">
Julien
</aside>
### config maps
<aside class="notes">
Sylvain
</aside>
### secrets
<aside class="notes">
Guillaume
</aside>
### pv/pvc
<aside class="notes">
Sylvain
</aside>
### contraintes
cpu / mem
<aside class="notes">
Jeff
</aside>
### infra as code
<aside class="notes">
Sylvain
</aside>

